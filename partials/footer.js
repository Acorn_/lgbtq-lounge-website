module.exports = function() {
    return `
<footer class="footer" style="border-top: 1px #ededed solid; background-color: #fff;  padding: 40px 0 70px 0">
    <div class="container">
        <div class="content has-text-centered">
            <strong>LGBTQ+ Lounge</strong> website by <a href="https://gitlab.com/Acorn_">Acorn</a>. <strong>Bulma</strong> by <a href="http://jgthms.com">Jeremy Thomas</a>.
        </div>
    </div>
</footer>
    `
}