module.exports = function(location, title) {
    return `
<title>LGBTQ+ Lounge &bull; ${title}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet"> 
<link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
<style>*{font-family: "Lato" sans-serif}.hero.is-medium .hero-body{padding-bottom:9rem;padding-top:9rem}</style>
    `
}