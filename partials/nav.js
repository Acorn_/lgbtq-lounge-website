module.exports = function(location) {
    return `
<nav class="navbar is-transparent" style="width: 100%; position: fixed; border-bottom: 1px #ededed solid; z-index: 100; transition: 0.3s; box-shadow: 0 -5px 8px #888888">
    <div class="container">
        <div class="navbar-brand">
            <img src="/assets/img/logo.png" style="height: 46px; margin: 3px">
            <div class="navbar-burger" onclick="document.getElementsByClassName('navbar-menu')[0].classList.toggle('is-active'); this.classList.toggle('is-active')">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="navbar-menu" style="box-shadow: none">
            <div class="navbar-end">
                <a class="navbar-item${(location == 'home') ? ' is-active' : ''}"${(location == 'home') ? '' : ' href="/"'}>Home</a>
                <a class="navbar-item${(location == 'staff') ? ' is-active' : ''}"${(location == 'staff') ? '' : ' href="/staff.html"'}>Staff</a>
                <a class="navbar-item" href="https://status.lgbtqlounge.com">Status</a>
                <a class="navbar-item">Subreddit</a>
                <a class="navbar-item" href="https://discord.gg/BJVqAXZ">Join!</a>
            </div>
        </div>
    </div>
</nav>
    `
}