var Discord   = require("discord.js"),
fs        = require("fs"),
tinycolor = require("tinycolor2"),
client    = new Discord.Client(),
quotes    = require("./quotes.json"),
processed = []

client.login(fs.readFileSync("./token").toString()) 

client.on("message", async (message) => {
    let args = message.content.split(" ").slice(1)
    if (message.content.split(" ")[0] = "!!") {
        if (message.author.id != "178409772436029440") return;
		let code = args.join(" ")
  		try {
      		let evaled = await eval(code);
			if (typeof evaled !== "string") evaled = require("util").inspect(evaled);
			evaled = evaled.replace(new RegExp(fs.readFileSync("./token").toString(), "g"), "The bot token was filtered for security reasons. ");
			message.channel.send(`\`\`\`xl\n${clean(evaled)}\n\`\`\``);
  		    }
  		catch(err) {
      		message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
  		    }
    }
})

function clean(text){if (typeof(text) === 'string') return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203)); else return text;} 