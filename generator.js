var Discord   = require("discord.js"),
    fs        = require("fs"),
    tinycolor = require("tinycolor2"),
    client    = new Discord.Client(),
    quotes    = require("./quotes.json"),
    pretty    = require("pretty"),
    processed = []

client.login(fs.readFileSync("./token").toString()) 

client.on("ready", () => {
    fs.writeFileSync("index.html", pretty(`
    <html>
        <head>
            ${require("./partials/head")("home", "Home")}
        </head>
        <body>
            ${require("./partials/nav")("home")}
            <section class="hero is-fullheight" style="border-bottom: 1px #ededed solid">
                <div class="hero-body">
                    <br>
                    <div class="container" style="text-align: center">
                        <div class="container" style="max-width: 400px; width: 100%">
                            <img src="/assets/img/logo.png" style="width: 100%; margin: 60px 0">
                        </div>
                        <br>
                        <div class="container">
                            <button class="button is-primary is-large" onclick="var ifr = document.getElementsByTagName('iframe')[0]; ifr.src = 'https://titanembeds.com/embed/339376237560463360'; document.getElementById('chat-modal').classList.add('is-active'); document.getElementsByTagName('html')[0].style['overflow-y'] = 'hidden'; this.setAttribute('onclick', 'document.getElementById(\\'chat-modal\\').classList.add(\\'is-active\\'); document.getElementsByTagName(\\'html\\')[0].style[\\'overflow-y\\'] = \\'hidden\\';')">Open Chat</button>
                        </div>
                        <div class="modal" id="chat-modal" style="z-index: 900">
                            <div class="modal-background"></div>
                            <div class="modal-content" style="width: 90%; z-index: 901">
                                <iframe style="width: 100%; max-width: 1200px; height: 600px; z-index: 902; border-radius: 9px" frameborder="0"></iframe>
                            </div>
                            <button class="modal-close is-large" onclick="document.getElementById('chat-modal').classList.remove('is-active'); ; document.getElementsByTagName('html')[0].style['overflow-y'] = ''"></button>
                        </div>
                    </div>
                    <br>
                </div>
            </section>
            <section class="hero is-primary is-bold">
                <div class="hero-body">
                    <div class="container">
                        <div class="columns" style="padding-top: 30px">
                            <div class="column has-text-centered">
                                <span class="icon fa fa-transgender-alt" style="font-size: 100px; margin: 20px 0 50px 0"></span>
                                <p>We respect everyone, of any sexuality or gender, from any country. We are LGBTQ+ friendly - homophobia, transphobia, racism, sexism and similar are strictly forbidden.</p>
                                <br>
                            </div>
                            <div class="column has-text-centered">
                                <span class="icon fa fa-star" style="font-size: 100px; margin: 20px 0 50px 0"></span>
                                <p>We only accept experienced and professional staff, all of our staff members are friendly and are here for you if you need any help.
                                </p>
                                <br>
                            </div>
                            <div class="column has-text-centered">
                                <span class="icon fa fa-users" style="font-size: 100px; margin: 20px 0 50px 0"></span>
                                <p>We currently have over <b>${Math.floor(client.guilds.get("339376237560463360").memberCount / 100) * 100} members<\/b> and our community is constantly growing. Why dont you come over and join in on the fun?</p>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            ${require("./partials/footer")()}
        </body>
    </html>
    `, {ocd: true}))
    fs.writeFileSync("staff.html", pretty(`
    <html>
        <head>
            ${require("./partials/head")("staff", "Staff Team")}
        </head>
        <body>
            ${require("./partials/nav")("staff")}
            <section class="hero is-medium" style="border-bottom: 1px #ededed solid">
                <div class="hero-body">
                    <br>
                    <div class="container" style="text-align: center">
                        <h1 class="title" style="font-size: 60pt; margin-bottom: 40px">Staff Team</h1>
                        <h6 class="subtitle"></h6>Take a look at our amazing staff team! </h6>
                    </div>
                </div>
            </section>
            <section class="section">
                ${generatePages("339377734532988961")}
                ${generatePages("339829428034994179")}
                ${generatePages("378281520768745483")}
                ${generatePages("371598633767927808")}
                ${generatePages("369590460660056064")}
                ${generatePages("386890140250800130")}
            </section>
            ${require("./partials/footer")()}
        </body>
    </html>
    `, {ocd: true}))
    process.exit()
    
    function generatePages(ID) {
        let members = client.guilds.get("339376237560463360").members.filter(e => e.roles.has(ID) && processed.indexOf(e.id) == -1)
        let column1arr = []
        let column2arr = []
        let column1 = ""
        let column2 = ""
        let i
        for (i = 0; i <= members.size; i = i + 2) {
            if (members.has(members.keyArray()[i])) {
                column1arr.push(members.get(members.keyArray()[i]))
                processed.push(members.get(members.keyArray()[i]).id)
            }
            if (members.has(members.keyArray()[i + 1])) {
                column2arr.push(members.get(members.keyArray()[i + 1]))
                processed.push(members.get(members.keyArray()[i + 1]).id)
            }    
        }
        column1arr.forEach(e => {
            if (e == undefined || !client.users.get(e.id)) return;
            column1 = column1 + 
            `
            <div class="box">
                <article class="media">
                    <figure class="media-left">
                        <p class="image is-64x64" style="border-radius: 50%">
                            <img src="${e.user.avatarURL}" style="width: 192px; border-radius: 50%;">
                        </p>
                    </figure>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>${e.displayName}</strong> <small>${e.user.username}#${e.user.discriminator}</small> <span class="tag is-rounded" style="color: ${(tinycolor(e.highestRole.hexColor).getLuminance() < 0.50) ? "#fff" : "#000"}; background-color: ${e.highestRole.hexColor}; font-weight: 600">${e.highestRole.name}</span>
                                <br>
                                ${quotes[e.id] || "<i>No quote provided.</i>"}
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            `
        })
        column2arr.forEach(e => {
            if (e == undefined || !client.users.get(e.id)) return;
            column2 = column2 + 
            `
            <div class="box">
                <article class="media">
                    <figure class="media-left">
                        <p class="image is-64x64" style="border-radius: 50%">
                            <img src="${e.user.avatarURL}" style="width: 192px; border-radius: 50%;">
                        </p>
                    </figure>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>${e.displayName}</strong> <small>${e.user.username}#${e.user.discriminator}</small> <span class="tag is-rounded" style="color: ${(tinycolor(e.highestRole.hexColor).getLuminance() < 0.50) ? "#fff" : "#000"}; background-color: ${e.highestRole.hexColor};font-weight: 600">${e.highestRole.name}</span>
                                <br>
                                ${quotes[e.id] || "<i>No quote provided.</i>"}
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            `
        })
        if (column1arr.length == 0) {
            column1 = `
            <div class="box">
                <article class="media">
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Currently, no members have this role :(</strong>
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            `
        }
        let columns = 
        `
            <div class="container">
                <br>
                <h1 class="title">${client.guilds.get("339376237560463360").roles.get(ID).name}</h1>
                <div class="columns">
                    <div class="column">
                        ${column1}
                    </div>
                    <div class="column">
                        ${column2}
                    </div>
                </div>
            </div>
        `
        columns = columns + "\n<br>"
        return columns
    }
})